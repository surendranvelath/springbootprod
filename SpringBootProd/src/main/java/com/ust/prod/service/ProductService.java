package com.ust.prod.service;

import java.util.List;

import com.ust.prod.entity.Product;

public interface ProductService {

	Product findProduct(Integer prodId);

	Product addProduct(Product prod);

	List<Product> getAllProducts();

	Product updateProduct(Product prod);

	Product deleteProduct(Integer prodId);

}
