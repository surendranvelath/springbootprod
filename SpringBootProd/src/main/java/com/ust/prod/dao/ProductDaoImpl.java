package com.ust.prod.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.ust.prod.entity.Product;
import com.ust.prod.exception.ProductException;
import com.ust.prod.util.JPAUtil;

@Repository("pDao")
public class ProductDaoImpl implements ProductDao {

	@Override
	public Product findProduct(Integer prodId) {
		Product prod = null;
		EntityManager em = JPAUtil.getEntityManager();
		prod = em.find(Product.class, prodId);
		if(prod==null) {
			throw new ProductException("Product Not found for ID: "+prodId);
		}
		return prod;
	}
	@Override
	public List<Product> getAllProducts() {
		EntityManager em = JPAUtil.getEntityManager();
		String qry = "select p from Product p";
		TypedQuery<Product> query = em.createQuery(qry, Product.class);
		List<Product> prodList = query.getResultList();
		return prodList;
	}
	
	@Override
	public Product addProduct(Product prod) {
		prod.setProdId(0);// in case id is not zero
		try {
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(prod);
		em.getTransaction().commit();
		}catch (Exception e) {
			e.printStackTrace();
			throw new ProductException("Failed to save Product");
		}
		return prod;
	}
	@Override
	public Product updateProduct(Product prod) {
		Product updatedProd = null;
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		updatedProd = em.find(Product.class, prod.getProdId());
		if(updatedProd!=null) {
			updatedProd.copy(prod);// managed bean is now dirty (not matching with database)
			em.merge(updatedProd);// now it is synced with database
			em.getTransaction().commit();
		}
		return updatedProd;
	}
	@Override
	public Product deleteProduct(Integer prodId) {
		Product deletedProd = null;
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		deletedProd = em.find(Product.class, prodId);
		if(deletedProd!=null) {
			em.remove(deletedProd);
			em.getTransaction().commit();
		}
		return deletedProd;
	}
}
