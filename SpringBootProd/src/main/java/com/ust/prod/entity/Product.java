package com.ust.prod.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
@Table(name = "prod_details")
public class Product {
	/*
	  {
	     "prodId":1234,
	     "prodName":"Laptop",
	     "quantity":4,
	     "price":45000.0,
	     "expiryDate":"28-Aug-2021"
	  }
	 */
	@Id
	@GeneratedValue
	private int prodId;
	private String prodName;
	private int quantity;
	private double price;
	@JsonFormat(pattern = "dd-MMM-yyyy", timezone = "Asia/Calcutta")
	private Date expiryDate;
	public void copy(Product prod) {
		prodId = prod.getProdId();
		prodName = prod.getProdName();
		quantity = prod.getQuantity();
		price = prod.getPrice();
		expiryDate = new Date(prod.getExpiryDate().getTime());// safe copy, as Date is mutable
	}
	public Product() {
		
	}
	public Product(int prodId, String prodName, int quantity, double price, Date expiryDate) {
		super();
		this.prodId = prodId;
		this.prodName = prodName;
		this.quantity = quantity;
		this.price = price;
		this.expiryDate = expiryDate;
	}
	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", prodName=" + prodName + ", quantity=" + quantity + ", price=" + price
				+ ", expiryDate=" + expiryDate + "]";
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	
}
