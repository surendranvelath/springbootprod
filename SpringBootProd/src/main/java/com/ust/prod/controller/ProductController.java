package com.ust.prod.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ust.prod.entity.Product;
import com.ust.prod.service.ProductService;

@RestController
@RequestMapping("/prod")
public class ProductController {
	@Autowired
	private ProductService pService;
	@GetMapping("/{prodid}")
//	public ResponseEntity<Product> findProduct(@PathVariable("prodid") Integer prodId){
	public Product findProduct(@PathVariable("prodid") Integer prodId){
		//Product prod = new Product(10005,"Laptop",5,45000,new Date(2021, 10, 11));
		Product prod = pService.findProduct(prodId);
//		if(prod==null) {
//			return new ResponseEntity("Sorry! Product Not found for "+prodId, HttpStatus.NOT_FOUND);
//		}
//		return new ResponseEntity(prod, HttpStatus.OK);
		return prod;
	}
	@PostMapping("/add")
	public Product addProduct(@RequestBody Product prod) {
//		Product addedProd = pService.addProduct(prod);
//		return addedProd;
		return pService.addProduct(prod);
	}
	@GetMapping("/all")
	public List<Product> getAllProducts(){
//		List<Product> prodList = pService.getAllProducts();
//		return prodList;
		return pService.getAllProducts();
	}
	
	@PutMapping("/update")
	public Product updateProduct(@RequestBody Product prod) {
		Product updatedProd = pService.updateProduct(prod);
		return updatedProd;
	}
	
	@DeleteMapping("/delete/{prodid}")
	public Product deleteProduct(@PathVariable("prodid") Integer prodId) {
		Product deletedProd = pService.deleteProduct(prodId);
		return deletedProd;		
	}
	@PostMapping("/addAll")
	public Product addProducts(@RequestBody List<Product> prodList) {
		System.out.println(prodList);
		return prodList.get(0);
	}
	
}

